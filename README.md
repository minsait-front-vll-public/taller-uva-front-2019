# taller-uva-front-2019

Repositorio para el taller del día 24 de febrero de 2020 sobre introducción a frameworks modernos con javascript: Angular y React

# Requisitos previos

## NodeJS
El entorno de desarrollo típico para front se basa en herramientas que utilizan NodeJS. NodeJS básicamente es un motor de javascript, que permite utilizar este lenguaje en diferentes entornos que no sean el navegador (por ejemplo en un servidor o en nuestros equipos). Deberemos, por tanto tener instalado NodeJS en nuestro ordenador para poder seguir el taller. Para ello, seguid los siguientes pasos:

* Vamos a la web de descarga de Node: [https://nodejs.org/es/](https://nodejs.org/es/)
* Descargamos la versión LTS (Long Term Support) para nuestro sistema operativo
* Instalamos...
* Una vez terminado, podemos comprobar las versiones de las utilidades que vamos a usar por línea de comandos:

`node -v`

`npm -v`

## IDE
Además de NodeJS, es recomendable tener un IDE de desarrollo. Podemos usar el bloc de notas o un editor sencillo de texto, pero creedme, usar un IDE aporta muchísimas ventajas. Un editor típico que solemos usar en entornos profesionales es Visual Studio Code (open source), aunque también podéis usar Sublime Text o Atom. Personalmente no os recomiendo usar Eclipse u otros IDES pensados para lenguajes como Java.

Descargad e instalad un IDE para javascript, el que más rabia os dé:

* [Visual Studio Code](https://code.visualstudio.com/)
* [Sublime Text](https://www.sublimetext.com/)
* [Atom] (https://atom.io/)

## Browser
Evidentemente, también usaremos un navegador. Excepto Internet Explorer / Edge nos vale cualquiera :) ([Chrome] (https://www.google.com/intl/es_es/chrome/), [Firefox] (https://www.mozilla.org/es-ES/firefox/new/) preferiblemente)

## Código fuente base

Para la demo, partiremos de un proyecto base que he creado para la ocasión. Para no perder tiempo e ir directos al grano, estaría muy bien que instaláseis las dependencias del proyecto antes del taller, ya que suele tardar un rato, dependiendo de vuestra conexión a internet.

Para bajar el proyecto lo podéis hacer de varias formas:

* Clonando este repositorio de git: [https://gitlab.com/minsait-front-vll-public/taller-uva-front-2019.git] (https://gitlab.com/minsait-front-vll-public/taller-uva-front-2019.git)
* Descargando / descomprimiendo el código fuente de [aquí] (https://gitlab.com/minsait-front-vll-public/taller-uva-front-2019/-/archive/master/taller-uva-front-2019-master.zip)
* Una vez hayáis clonado / descargado el proyecto, desde una consola de comandos, accedéis a la carpeta del proyecto y ejecutáis los siguientes comandos

`npm run instalar`

Y ahí comenzamos ;)